#include "ipbase.ih"

void IPbase::byteCountsData(Record const &record) const
{
    d_logDataStream <<
        setw(11) << record.sentBytes()      << ',' <<
        setw(11) << record.receivedBytes()  << ',';
}

