#include "ipbase.ih"

IPbase::IPbase(std::ostream &stdMsg, std::ostream &logDataStream)
:
    d_stdMsg(stdMsg),
    d_logDataStream(logDataStream) 
{
    Options const &options = Options::instance();

                                        // no-via spec'd    no-via not spec'd
    if (options.showVia())
    {
        d_via     = &IPbase::via;
        d_viaData =  &IPbase::viaData;
    }
    else
    {
        d_via     = &IPbase::ignore;
        d_viaData =  &IPbase::ignore;
    }

    if (options.showDst())
    {
        d_dst     = &IPbase::dst;
        d_dstData =  &IPbase::dstData;
    }
    else
    {
        d_dst     = &IPbase::ignore;
        d_dstData =  &IPbase::ignore;
    }

    if (options.byteCounts())
    {
        d_byteCounts     = &IPbase::byteCounts;
        d_byteCountsData =  &IPbase::byteCountsData;
    }
    else
    {
        d_byteCounts     = &IPbase::ignore;
        d_byteCountsData =  &IPbase::ignore;
    }


    d_byteCounts = options.byteCounts() ? 
                        &IPbase::byteCounts : &IPbase::ignore;

    d_logData = not options.logData().empty() ?
                        &IPbase::logData : &IPbase::ignore;
}




