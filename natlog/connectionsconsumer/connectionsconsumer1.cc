#include "connectionsconsumer.ih"

ConnectionsConsumer::ConnectionsConsumer(ostream &stdMsg, Storage &storage)
:
    d_logDataStream(header),
    d_stdMsg(stdMsg),
    d_storage(storage),
    d_icmp(stdMsg, d_logDataStream),
    d_udp(stdMsg, d_logDataStream),
    d_tcp(stdMsg, d_logDataStream),
    d_handler
    {
        { Record::ICMP,     &d_icmp }, 
        { Record::TCP,      &d_tcp  }, 
        { Record::UDP,      &d_udp  }, 
    },
    d_ttl(Options::instance().ttl())
{
    if (
        string logData = Options::instance().logData();
        not logData.empty()
    )
        d_logDataStream.open(logData, false);       // prepare for data log

    Signal::instance().add(SIGINT, *this);
    Signal::instance().add(SIGTERM, *this);
}


