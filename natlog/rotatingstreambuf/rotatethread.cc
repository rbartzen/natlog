#include "rotatingstreambuf.ih"

// static
void RotatingStreambuf::rotateThread()
{
    Options const &options = Options::instance();

    size_t freq = options.rotateFreq();

    if (freq == 0)                              // no rotation
        return;

    freq *= options.rotateFactor();


    while (s_semaphore.wait_for(chrono::minutes(freq)) == cv_status::timeout)
        rotate();
}
