#include "rotatingstreambuf.ih"

// static
void RotatingStreambuf::rotate(RotationInfo &info)
{
    if (info.rotationStreambuf == 0)
        return;

    lock_guard<mutex> lg{ info.rotationMutex };

    size_t seconds = time(0);

    if (seconds < info.lastRotation + Options::instance().rotationInterval())
        return;                                 // skip the rotation if the

                                                // previous one just ended
    info.lastRotation = seconds;

    info.rotationStreambuf->rotate(Options::instance().nRotations());
}





