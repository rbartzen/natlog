#include "rotatingstreambuf.ih"

void RotatingStreambuf::notify()
{
    if (rotationsRequested())
    {
        s_semaphore.notify();
        s_rotateThread.join();
    }
}
       
