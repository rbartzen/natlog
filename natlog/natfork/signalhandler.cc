#include "natfork.ih"

    // only one signal is received. No need to check the signal

void NatFork::signalHandler(size_t signum)
{
    if (signum == SIGHUP)
        RotatingStreambuf::logRotate();
    else                                    // signal may also be SIGALRM,
        RotatingStreambuf::dataRotate();    // only 2 signals: no check needed
}
