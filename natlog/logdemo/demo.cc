#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

uint32_t hostlong = (192 << 24) + (168 << 16) + (17 << 8) + 6;
string ipstr = "192.168.017.006";
string timestr = "Jun  3 00:00:00:000000";
string portstr = "12345";
size_t seconds = 1654252885;

// input: arg1 d: dst data, v: via data, dv: beide
int main(int argc, char **argv)
{
    if (argc == 1)
        return 0;

    string arg1 = argv[1];

    bool dst = arg1.find('d') != string::npos;
    bool via = arg1.find('v') != string::npos;


// TCP/UDP output   IPbase::logconnection

    cout << "from " << timestr << 
        " thru " << timestr << 
        " (UTC)" << ": " << 
        " udp" << ' ' <<
        ipstr << ',' << left << setw(5) << portstr << "";
                                                    // 5 - portlength;

    if (via)
        //(this->*d_via)(record);
        cout << " (via: " << ipstr  << ',' << 
                                setw(5) << portstr << ')';

    if (dst)
        //(this->*d_dst)(record);
        cout << " "
                "to " << ipstr << ',' << portstr << ';' << "";
                                                        // 5 - portlength
    cout << right;

        //(this->*d_byteCounts)(record);
    cout << " "
            "sent: "     << setw(10) << 0 << ", "
            "received: " << setw(10) << 0;
        

    cout  << endl;              // logtypetext (default: empty)

    // logCSV


// ICMP output:

    cout << "from " << timestr << 
        " thru " << timestr << 
        " (UTC)" << ": " << 
        "icmp" << ' ' <<
        ipstr << "      ";


    // (this->*d_via)(record);
    if (via)
        //(this->*d_via)(record);
        cout << " (via: " << ipstr  << ") " << "     ";
                                                // 5 - portlength
    if (dst)
        cout << " "
                "to " << ipstr << "; " << "     ";
                                         // 5 - portlength

    //(this->*d_byteCounts)(record);

    cout << " "
            "sent: "     << setw(10) << 0 << ", "
            "received: " << setw(10) << 0;

    cout << endl;           // logtype

    // logCSV


    // ConnectionsConsumer::header(ostream &log)
    cout << "\n"
            "====================================================\n"
                    "type," <<

        setw(11) << "srcNr"     << ',' <<
        setw(16) << "srcIP"     << ',' <<
        setw(8)  << "srcPort"   << ',';

    if (dst)
        cout << 
            setw(11) << "dstNr"     << ',' <<
            setw(16) << "dstIP"     << ',' <<
            setw(8)  << "dstPort"   << ',';

    if (via)
        cout << 
            setw(11) << "viaNr"     << ',' <<
            setw(16) << "viaIP"     << ',' <<
            setw(8)  << "viaPort"   << ',';

    cout <<
        setw(11) << "sent"      << ',' <<
        setw(11) << "recvd"     << ',' <<

        setw(11) << "begin"     << ',' <<
        setw(11) << "end"       << ", " <<

        setw(22) << "beginTime" << ", " <<
        setw(22) << "endTime"   << ",     status" << endl;
                                 //   1234567890

// logCSV calls IPbase::d_logData calls IPBase::logData

    cout << 
                        "icmp"  << ',' <<
            setw(11) << hostlong<< ',' <<
            setw(16) << ipstr    << ',' <<
            setw(8)  << 0           << ',';

    if (dst)
    {
        cout <<
            setw(11) << hostlong<< ',' <<
            setw(16) << ipstr    << ',' <<
            setw(8)  << 0           << ',';
    }

    if (via)
    {
        cout <<
            setw(11) << hostlong << ',' <<
            setw(16) << ipstr    << ',' <<
            setw(8)  << 0        << ',';
    }

    cout << 
            setw(11) << 0      << ',' <<
            setw(11) << 0  << ',' <<

            setw(11) << seconds      << ',' <<
            setw(11) << seconds        << ", " <<
                         timestr     << ", " <<
                         timestr       << ", " <<

            setw(10) << "" << endl;

    cout << 
                        " udp"  << ',' <<
            setw(11) << hostlong<< ',' <<
            setw(16) << ipstr    << ',' <<
            setw(8)  << portstr     << ',';

    if (dst)
    {
        cout <<
            setw(11) << hostlong<< ',' <<
            setw(16) << ipstr    << ',' <<
            setw(8)  << portstr     << ',';
    }

    if (via)
    {
        cout <<
            setw(11) << hostlong << ',' <<
            setw(16) << ipstr    << ',' <<
            setw(8)  << portstr     << ',';
    }

    cout << 
            setw(11) << 0      << ',' <<
            setw(11) << 0  << ',' <<

            setw(11) << seconds      << ',' <<
            setw(11) << seconds        << ", " <<
                         timestr     << ", " <<
                         timestr       << ", " <<

            setw(10) << "" << endl;


}
    
