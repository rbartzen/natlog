#include "options.ih"

size_t Options::readPid() const
{
    if (d_PIDfile.empty())
        throw Exception{} << "No PID file available: no daemon?";

    ifstream in{ d_PIDfile };
    size_t pid;
    if (not (in >> pid))
        throw Exception{} << "No valid PID file " << d_PIDfile << " found";

    return pid;
}
