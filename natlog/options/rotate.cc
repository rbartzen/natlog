#include "options.ih"

bool Options::rotate() const
{
    if (d_rotate)
        ::kill(readPid(), SIGHUP);

    return d_rotate;
}
