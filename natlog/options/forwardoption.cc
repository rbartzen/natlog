#include "options.ih"

bool Options::forwardOption() const
{
    if (kill())
        return true;

    bool rotateRequest = rotate();

    return rotateData() or rotateRequest;
}
