inline bool Options::byteCounts() const
{   
    return d_byteCounts;
}

inline Options::Mode Options::mode() const
{   
    return d_mode;
}

inline bool Options::realTime() const
{
    return d_mode != TCPDUMP;
}

inline size_t Options::rotationInterval() const
{
    return d_rotationInterval;
}

inline bool Options::stdout() const
{   
    return d_stdout;
}

inline bool Options::debug() const
{   
    return d_debug;
}

inline bool Options::showDst() const
{   
    return d_showDst;
}

inline bool Options::showVia() const
{   
    return d_showVia;
}

inline size_t Options::verbose() const
{   
    return d_verbose;
}

inline bool Options::daemon() const
{   
    return d_daemon;
}

inline void Options::foreground()
{   
    d_daemon = false;
}

inline std::string const &Options::log() const
{   
    return d_log;
}

inline size_t Options::rotateFreq() const
{
    return d_rotateFreq;
}

inline size_t Options::rotateFactor() const
{
    return d_rotateFactor;
}

inline std::string const &Options::rotateTimeSpec() const
{
    return d_rotateTimeSpec;
}

inline size_t Options::nRotations() const
{
    return d_nRotations;
}

inline time_t Options::ttl() const
{
    return d_ttl;
}

inline time_t Options::ttlTCP() const
{
    return d_ttlTCP;
}

inline size_t Options::IPheaderSize() const
{
    return d_IPheaderSize;
}

inline Options::Time Options::time() const
{   
    return d_time->second;
}

inline std::string const &Options::timeTxt() const
{   
    return d_time->first;
}

inline std::string const &Options::configPath() const
{   
    return d_configPath;
}

inline std::string const &Options::syslogTag() const
{   
    return d_syslogTag;
}

inline std::string const &Options::logData() const
{   
    return d_logData;
}

//inline size_t Options::logDataFlush() const
//{   
//    return d_logDataFlush;
//}

inline std::string const &Options::pidFile() const
{   
    return d_PIDfile;
}

inline std::string const &Options::timeSpecError() const
{   
    return d_timeSpecError;
}

inline FBB::Priority Options::syslogPriority() const
{   
    return d_syslogPriority->second;
}

inline std::string const &Options::priority() const
{   
    return d_syslogPriority->first;
}

inline std::string Options::syslogPriorityError() const
{   
    return d_syslogPriorityError;
}

inline FBB::Facility Options::syslogFacility() const
{   
    return d_syslogFacility->second;
}

inline std::string Options::syslogFacilityError() const
{   
    return d_syslogFacilityError;
}

inline char const *Options::defaultPIDfile() 
{
    return s_defaultPIDfile;
}

inline char const *Options::defaultConfigPath() 
{
    return s_defaultConfigPath;
}

inline std::string const &Options::facility() const
{   
    return d_syslogFacility->first;
}

inline std::string const &Options::conntrackCommand() const
{
    return d_conntrackCommand;
}

inline char const *Options::conntrackDevice() const
{
    return d_conntrackDevice.c_str();
}

inline char const *Options::operator[](size_t idx) const
{
    return d_arg[idx];
}

inline size_t Options::nArgs() const
{
    return d_arg.nArgs();
}

inline std::string const &Options::basename() const
{
    return d_arg.basename();
}

inline char const *Options::defaultConntrackArgs()
{
    return s_defaultConntrackArgs;
}

inline char const *Options::defaultConntrackCommand() 
{
    return s_defaultConntrackCommand;
}

inline char const *Options::defaultConntrackDevice() 
{
    return s_defaultConntrackDevice;
}

inline char const *Options::defaultSyslogIdent() 
{
    return s_defaultSyslogIdent;
}

inline char const *Options::defaultSyslogFacility() 
{
    return s_defaultSyslogFacility;
}

inline char const *Options::defaultSyslogPriority() 
{
    return s_defaultSyslogPriority;
}

inline size_t Options::conntrackRestart() const
{
    return d_conntrackRestart;
}

inline std::unordered_set<IP_Types::Protocol> const &Options::protocolSet() 
                                                                        const
{
    return d_protocols;
}

inline std::string const &Options::protocolName(Protocol protocol)
{
    return s_protocol2name.find(protocol)->second;
}

inline bool Options::hasProtocol(size_t protocol) const
{
    return d_protocols.find(static_cast<Protocol>(protocol))
            != d_protocols.end(); 
}
