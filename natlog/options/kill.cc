#include "options.ih"

bool Options::kill() const
{
    if (d_terminate)
    {
        if (::kill(readPid(), SIGTERM) != 0)
            throw Exception{} << "Terminating " << d_arg.basename() << 
                " failed: " << errnodescr;
    }

    return d_terminate;
}
