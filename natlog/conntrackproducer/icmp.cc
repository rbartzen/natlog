#include "conntrackproducer.ih"

bool ConntrackProducer::icmp(string const &line)     
{
    if (not (s_icmp << line))           // this is not an ICMP connection
        return false;                   // (maybe there are other than icmp,
                                        // tcp and udp connections, hence the
                                        // check)

        // if not requested then notUsed() instead of icmp() is called
    d_storage.push( new ConntrackRecord{ Record::ICMP, s_icmp } );
    return true;                    // not used (see process())
}
