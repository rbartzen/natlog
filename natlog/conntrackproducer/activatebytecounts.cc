#include "conntrackproducer.ih"

void ConntrackProducer::activateByteCounts() const
{
    ofstream out = Exception::factory<ofstream>(s_nfConntrackAcct,
                                                ios::in | ios::out);
    out.put('1');
}
